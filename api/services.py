import requests


SERVICE_URL = 'https://www.decodethis.com/webservices/decodes/{vin}/{api_key}/1.json'


def get_decoded_vehicle(request: "<class 'rest_framework.request.Request'>") -> tuple:
    """Parse vehicle with the given VIN.
    Return tuple containing status code and data.
    """
    api_key, vin = request.data.get('api_key'), request.data.get('vin')
    if api_key is None or vin is None:
        return requests.codes.bad, None

    url = SERVICE_URL.format(vin=vin, api_key=api_key)
    response = requests.get(url)
    data = response.json()

    if vin_is_invalid(data) or response.status_code != requests.codes.ok:
        return requests.codes.bad, None
    else:
        return requests.codes.ok, parse(data)


def vin_is_invalid(data: dict) -> bool:
    """If `Valid` parameter of response text is False, then VIN is considered to be invalid."""
    return data['decode']['Valid'] == 'False'


def parse(data: dict) -> dict:

    def get_parameter(paramname):
        nonlocal equipment_data
        for item in equipment_data:
            if item.get('name') == paramname:
                return item.get('value')

    general_data = data['decode']['vehicle'][0]
    equipment_data = data['decode']['vehicle'][0]['Equip']

    new_data = {
        'vin': data['decode'].get('VIN'),
        'year': general_data.get('year'),
        'make': general_data.get('make'),
        'model': general_data.get('model'),
        'type': general_data.get('engine'),
        'weight': get_parameter('Curb Weight-manual'),
        'dimensions': {
            'length': get_parameter('Overall Length'),
            'width': get_parameter('Overall Width'),
            'height': get_parameter('Overall Height'),
        }
    }
    return new_data
