from django.http import Http404

from rest_framework import generics
from rest_framework import mixins
from rest_framework import status
from rest_framework.response import Response

from .models import Vehicle
from .serializers import VehicleSerializer
from .services import get_decoded_vehicle


def create_vehicle(instance, request, *args, **kwargs):
    status_code, data = get_decoded_vehicle(request)
    if status_code == status.HTTP_200_OK:
        request._full_data = data
        return instance.create(request, *args, **kwargs)
    else:
        return Response(status=status.HTTP_400_BAD_REQUEST)


class VehicleListCreate(generics.ListCreateAPIView):
    lookup_field = 'vin'
    queryset = Vehicle.objects.all()
    serializer_class = VehicleSerializer

    def post(self, request, *args, **kwargs):
        return create_vehicle(self, request, *args, **kwargs)


class VehicleGetDeleteUpdate(generics.RetrieveUpdateDestroyAPIView,
                             mixins.CreateModelMixin):
    lookup_field = 'vin'
    queryset = Vehicle.objects.all()
    serializer_class = VehicleSerializer

    def put(self, request, *args, **kwargs):
        try:
            return self.update(request, *args, **kwargs)
        except Http404:
            request._full_data = {
                'api_key': request.data.get('api_key'),
                'vin': request.path.split('/api/vehicles/')[1].strip('/')
            }
            return create_vehicle(self, request, *args, **kwargs)
