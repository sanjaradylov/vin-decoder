import abc

from django.urls import include, path, reverse

from rest_framework import status
from rest_framework.test import APITestCase, URLPatternsTestCase

from .models import Vehicle

from vin_decoder.local_settings import DECODE_API_KEY


TEST_DATA = [
    {
        'vin': '1P3EW65F4VV300946',
        "make": "Plymouth",
        "model": "Prowler",
        "type": "3.5L V6 SOHC 24V",
        "year": "1997",
    },
    {
        'vin': '1FADP5AU5JL102671',
        "make": "Ford",
        "model": "C-Max Hybrid",
        "type": "2.0L L4 DOHC 16V HYBRID",
        "year": "2018",
    },
]


class VehicleTestABC(metaclass=abc.ABCMeta):
    urlpatterns = [
        path('api/', include('api.urls')),
    ]

    def create_and_decode(self, vin) -> "<class 'rest_framework.response.Response'>":
        """Create a new vehicle with the given VIN and save it temporarily to a DB."""
        url = reverse('vehicle-list')
        data = {'vin': vin, 'api_key': DECODE_API_KEY}
        response = self.client.post(url, data)
        return response

    def setUp(self):
        pass

    @abc.abstractmethod
    def test_vehicle_create(self):
        """Test POST method (using Decode This API, create a new vehicle with the given VIN)."""
        raise NotImplementedError()

    @abc.abstractmethod
    def test_vehicle_list(self):
        """Test GET method (list all the vehicles in DB)."""
        raise NotImplementedError()

    @abc.abstractmethod
    def test_vehicle_put(self):
        """Test PUT method (create a new vehicle or update an existing)."""
        raise NotImplementedError()

    @abc.abstractmethod
    def test_vehicle_get(self):
        """Test GET method (get a vehicle by its VIN)."""
        raise NotImplementedError()
    
    @abc.abstractmethod
    def test_vehicle_delete(self):
        """Test DELETE method (delete a vehicle by its VIN)."""
        raise NotImplementedError()


class VehicleTestsOK(APITestCase, URLPatternsTestCase, VehicleTestABC):
    """Defines methods that obtain permissible urls and execute HTTP methods successfully."""
    def setUp(self):
        self.vehicle = TEST_DATA[0]
        self.url_vehicle_list = reverse('vehicle-list')
        self.url_vehicle_detail = reverse('vehicle-detail', args=[self.vehicle['vin']])
        # Create a new vehicle and use it while testing all provided methods
        self.response = self.create_and_decode(self.vehicle['vin'])

    def test_vehicle_create(self):
        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(self.response.data), 7)

        vehicle_object = Vehicle.objects.get(vin=self.vehicle['vin'])
        for key, value in self.vehicle.items():
            self.assertEqual(getattr(vehicle_object, key), value)

    def test_vehicle_list(self):
        another_vehicle = TEST_DATA[1]
        self.create_and_decode(another_vehicle['vin'])

        response = self.client.get(self.url_vehicle_list)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), len(TEST_DATA))

        for vehicle in self.vehicle, another_vehicle:
            vehicle_object = Vehicle.objects.get(vin=vehicle['vin'])
            for key, value in vehicle.items():
                self.assertEqual(getattr(vehicle_object, key), value)

    def test_vehicle_put(self):
        # New vehicle parameters
        another_vehicle = TEST_DATA[1]
        url = reverse('vehicle-detail', args=[another_vehicle['vin']])
        data = {'api_key': DECODE_API_KEY}
        # We haven't created the vehicle with the given VIN yet,
        # so PUT will create it and store in DB.
        response = self.client.put(url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        # Now DB stores our vehicle and we can update its parameters.
        data.pop('api_key') # No need for api_key anymore.
        data.update({
            'make': 'Sausage',
            'model': 'Spam',
            'type': 'Shrubbery',
        })
        response = self.client.put(url, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        vehicle_object = Vehicle.objects.get(vin=another_vehicle['vin'])
        for key, value in data.items():
            self.assertEqual(getattr(vehicle_object, key), value)

    def test_vehicle_get(self):
        response = self.client.get(self.url_vehicle_detail)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 7)

    def test_vehicle_delete(self):
        response = self.client.delete(self.url_vehicle_detail)

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(Vehicle.objects.filter(vin=self.vehicle['vin']).exists(), False)


class VehicleTestsBad(APITestCase, VehicleTestABC):
    """Defines methods that test bad requests."""
    def setUp(self):
        self.bad_vin = 'SPAM'
        self.ok_vin = TEST_DATA[0]['vin']
        self.ok_but_not_created_vin = TEST_DATA[1]['vin']
        self.bad_url_vehicle_detail = reverse('vehicle-detail', args=[self.ok_but_not_created_vin])

    def test_vehicle_create(self):
        response = self.create_and_decode(self.bad_vin)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_vehicle_list(self):
        response = self.client.get(reverse('vehicle-list'))
        self.assertEqual(len(response.data), 0)

    def test_vehicle_put(self):
        # Inappropriate VIN
        response = self.client.put(self.bad_vin, {'api_key': DECODE_API_KEY})
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        # Inappropriate API Key
        response = self.client.put(self.ok_vin, {'api_key': 'SPAM'})
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_vehicle_get(self):
        response = self.client.get(self.bad_url_vehicle_detail)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_vehicle_delete(self):
        response = self.client.delete(self.bad_url_vehicle_detail)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
