from django.db import models
from django.contrib.postgres.fields import JSONField


class Vehicle(models.Model):
    vin = models.CharField(null=True, blank=True, unique=True, max_length=20)
    year = models.CharField(null=True, blank=True, max_length=4)
    make = models.CharField(null=True, blank=True, max_length=255)
    model = models.CharField(null=True, blank=True, max_length=255)
    type = models.CharField(null=True, blank=True, max_length=255)
    weight = models.CharField(null=True, blank=True, max_length=7)
    dimensions = JSONField(default={}, blank=True)

    def __str__(self):
        return self.vin
