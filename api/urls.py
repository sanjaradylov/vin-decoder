from django.urls import path

from rest_framework.urlpatterns import format_suffix_patterns

from . import views


urlpatterns = [
    path('vehicles/', views.VehicleListCreate.as_view(), name='vehicle-list'),
    path('vehicles/<vin>/', views.VehicleGetDeleteUpdate.as_view(), name='vehicle-detail'),
]
urlpatterns = format_suffix_patterns(urlpatterns)
