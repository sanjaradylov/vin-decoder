from rest_framework import serializers

from api.models import Vehicle


class VehicleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vehicle
        fields = ('vin', 'year', 'make', 'model', 'type', 'dimensions', 'weight')
