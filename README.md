**VIN Decoder**

REST-based microservice that manages vehicle data with the provided [VIN](https://en.wikipedia.org/wiki/Vehicle_identification_number).
Exposes [Decode This API](https://www.decodethis.com/blog/api_documentation).

---

## Install dependencies

It is recommended to use [virtual environments](https://virtualenv.pypa.io/en/stable/).
Go to the main directory and place a new virtual environment:
```
$ cd vin_decoder
$ virtualenv venv
```

Activate the virtual environment:
```
$ source venv/bin/activate
```

Install required packages using [pip3](https://pip.pypa.io/en/stable/):
```
$ pip3 install -r requirements.txt
```

---

## Configure and run

Create file ```vin_decoder/local_settings.py``` and edit your local settings. Note that in order to run tests, you must create ```DECODE_API_KEY``` variable in local settings and assign it to an appropriate decode key.

Apply migrations and create a superuser:
```
$ python3 manage.py migrate
$ python3 manage.py createsuperuser
```

Run the project:
```
$ python3 manage.py runserver
```

---

## Test

Run unittests (see ```api/tests.py```):
```
$ python3 manage.py test api
```

or test manually using [httpie](https://httpie.org/). Here are the available HTTP methods and the usage:

GET (List)
```
$ http POST http://127.0.0.1:8000/api/vehicles/
```

GET
```
$ http POST http://127.0.0.1:8000/api/vehicles/{vin}
```

POST
```
$ http POST http://127.0.0.1:8000/api/vehicles/ vin=VIN api_key=YOUR-DECODE-API-KEY
```

DELETE
```
$ http DELETE http://127.0.0.1:8000/api/vehicles/{vin}
```

PUT (Update)
```
$ http POST http://127.0.0.1:8000/api/vehicles/{vin} [key=value]...
```
PUT (Create)
```
$ http POST http://127.0.0.1:8000/api/vehicles/{vin} api_key=YOUR-DECODE-API-KEY [key=value]...
```

---

An HTTP POST request example:
```
$ http POST http://127.0.0.1:8000/api/vehicles/ vin=1P3EW65F4VV300946 api_key=YOUR-DECODE-API-KEY
```
Output:
```json
HTTP/1.1 201 Created
Allow: GET, POST, HEAD, OPTIONS
Content-Length: 185
Content-Type: application/json
Date: Sat, 18 Aug 2018 11:55:52 GMT
Server: WSGIServer/0.2 CPython/3.4.2
Vary: Accept, Cookie
X-Frame-Options: SAMEORIGIN

{
    "dimensions": {
        "height": "50.90",
        "length": "165.30",
        "width": "76.50"
    },
    "make": "Plymouth",
    "model": "Prowler",
    "type": "3.5L V6 SOHC 24V",
    "vin": "1P3EW65F4VV300946",
    "weight": "2862",
    "year": "1997"
}
```